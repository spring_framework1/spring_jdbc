package com.spring.jdbc.jdbc;

import java.util.List;
import java.util.Scanner;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

import com.spring.jdbc.jdbc.dao.StudentDao;
import com.spring.jdbc.jdbc.entites.Student;

public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Program has Started :)");
        
        //ApplicationContext context = new ClassPathXmlApplicationContext("com/spring/jdbc/jdbc/config.xml");
        ApplicationContext context = new AnnotationConfigApplicationContext(JdbcConfig.class);
        
        
        /*
        JdbcTemplate templete = context.getBean("jdbcTemplete", JdbcTemplate.class);
        
        //insert query
        String query = "INSERT INTO student(id, name,address) VALUES(?, ?, ?)";
        
        //fire the query
        int result = templete.update(query, 3,"Onkar Agawane", "Solpaur");
        System.out.println("Number of records inserted: "+result);
      
         */
        
        StudentDao studentDao = context.getBean("studentDao", StudentDao.class);
        
        
        //Student student = new Student();
        
        //System.out.println("Enter Student Name, Id and Address: ");  //For Insert Operation
        //System.out.println("Enter Student Name, Id and Address: ");  //For Update/Change Operation
        //System.out.println("Enter ID of Student to Delete the data: ");  //or Delete Operation
        //System.out.println("Enter ID of the Student of which data you want to featch: "); //For Select single Data
        //Scanner sc = new Scanner(System.in);
        //String name = sc.nextLine();
        //int id = sc.nextInt();
        //String address = sc.next();
        
        //student.setId(id);
        //student.setAddress(address);
        
        //****************For Insert Operation***************
        //int result = studentDao.insert(student);  
        //System.out.println("Number of Students Added: "+result);
        
        //****************For Update/Change Operation***************
        //int result = studentDao.change(student);  
        //System.out.println("Information Updated: "+result);
        
        //****************For Delete Operation****************
        //int result = studentDao.delete(id());  // for this we need only id so we pass only id
        //System.out.println("Number of Student Deleted: "+result);
        
        //****************For Select single Row***************
        //Student student1 = studentDao.getStudent(id);
        //System.out.println(student1);
        
      //****************For Select single Multiple Row***************
        List<Student> student2 = studentDao.getAllStudents();
        //System.out.println(student2); //we can use for each loop for printing data also
        
        for(Student s:student2)
        {
        	System.out.println(s);
        	
        }
        
    }
}
