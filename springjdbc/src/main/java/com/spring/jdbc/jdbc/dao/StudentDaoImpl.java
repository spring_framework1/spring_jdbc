package com.spring.jdbc.jdbc.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.spring.jdbc.jdbc.entites.Student;

@Component("studentDao")
public class StudentDaoImpl implements StudentDao{

	@Autowired
	private JdbcTemplate jdbcTemplete;
	
	public int insert(Student student) {
		//insert query
        String query = "INSERT INTO student(id, name,address) VALUES(?, ?, ?)";

        int r = this.jdbcTemplete.update(query, student.getId(), student.getName(), student.getAddress());
		return r;
	}
	
    public int change(Student student) {
		//updating data
    	
    	String query = "UPDATE student SET name=?, address=? WHERE id = ? ";
    	
    	int r = this.jdbcTemplete.update(query, student.getName(), student.getAddress(), student.getId());
		
    	return r;
	}
	
	public int delete(int studentId) {
		//Delete Query
		
		String query = "DELETE FROM student WHERE id=? ";
		int r = this.jdbcTemplete.update(query, studentId);
		return r;
	}

	public Student getStudent(int studentId) {
		//select single student data
		String query = "SELECT * FROM student WHERE id = ?";
		RowMapper<Student> rowMapper = new RowMapperImpl();  // assigning the object of the RowMapperImpl to its parent RowMapper<> 
		Student student = this.jdbcTemplete.queryForObject(query,  rowMapper, studentId);
      //Student student = this.jdbcTemplete.queryForObject(query, new RowMapperImpl, studentId); // we can use this also didn't understand why we need to perform line number 45
		
		
//		Student student = this.jdbcTemplete.queryForObject(query, new RowMapper() {
//
//			public Object mapRow(ResultSet (hrs), (dint) rowNum) throws SQLException {
//				
//				Student student = new Student();
//				student.setId(rs.getInt(1));
//				student.setName(rs.getString(2));
//				student.setAddress(rs.getString(3));        //*******This all commented part is using anonymous class of RowMapper
//				
//				return student;
//			}		
//		}, studentId);
		
		return student;
	}
	
	public List<Student> getAllStudents() {
		//Selecting All/Multiple Students
		
		String query = "SELECT * FROM student";
		List<Student> students = this.jdbcTemplete.query(query, new RowMapperImpl());
		
		return students;
	}

	public JdbcTemplate getJdbcTemplete() {
		return jdbcTemplete;
	}

	@Autowired
	public void setJdbcTemplete(JdbcTemplate jdbcTemplete) {
		this.jdbcTemplete = jdbcTemplete;
	}







	
	

}
